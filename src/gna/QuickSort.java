package gna;

/**
 * Performs sort by using the Selection Sort algorithm.
 *
 * @author Thomas De Clercq
 */
public class QuickSort implements libpract.SortingAlgorithm{
  /**
   * Sorts the given array using quick sort.
   * 
   * @return The number of comparisons (i.e. calls to compareTo)
   * performed by the algorithm.
   */
	public Comparable sortedArray[];
	public <T extends Comparable<T>> int sort(T[] array) {
		//shuffle(array); 			//libpract.providedtests does not allow a shuffle
		sort(array, 0, array.length - 1);
		sortedArray = new Comparable[array.length];
		sortedArray = array.clone();
		print(array);
		return getCountComparisons();
	}
	  private static <T> void shuffle(T[] array) {
	        int N = array.length;
	        for (int i = 0; i < N; i++) {
	            int r = i + uniform(N-i);     
	            T temp = array[i];
	            array[i] = array[r];
	            array[r] = temp;
	        }
	    }
  
  private static int uniform(int N) {
		return (int) (Math.random() * N);
	}
private int getCountComparisons() {
	int comparisonsresult = comparisons;
	comparisons = 0;
	return comparisonsresult;
}

public <T extends Comparable<T>> void sort(T[] array, int lo, int hi) {
	  if (hi <= lo) return;
	  int j = partition(array,lo,hi);
	  sort(array, lo, j-1);
	  sort(array, j+1, hi);
  }
  private static <T extends Comparable<T>> boolean less(T v, T w){
	  comparisons++;
	  return v.compareTo(w) < 0;
	  
  }
  private static <T extends Comparable<T>> void exch
  (T[] array, int i, int j){
	  T t = array[i]; 
	  array[i] = array[j];
	  array[j] = t;
  }
  private static <T extends Comparable<T>> int partition
  (T[] array, int lo, int hi){
	  int i = lo, j = hi+1;
	  T v = array[lo];
	  while (true){
		  while (less(array[++i],v)) if (i == hi) break;
		  while (less(v, array[--j])) if (j == lo) break;
		  if (i >=j) break;
		  exch(array,i,j);
	  }
	  exch(array, lo, j);	  
	  return j;
  }
  private static <T extends Comparable<T>> void print(T[] array){
	  
	  for (int i=0; i< array.length; i++){
		  
		  System.out.print(array[i]);
		  System.out.print(" ");
	  }
	  System.out.println();
		  
  }
  private static int comparisons;
  /**
   * Constructor.
   */
  public QuickSort(){
	  
  }
}
