package gna;

/**
 * Performs sort by using the Selection Sort algorithm.
 *
 * @author Thomas De Clercq
 */
public class SelectionSort implements libpract.SortingAlgorithm{
  /**
   * Sorts the given array using selection sort.
   * 
   * @return The number of comparisons (i.e. calls to compareTo)
   * performed by the algorithm.
   */
  public Comparable sortedArray[];
  public <T extends Comparable<T>> int sort(T[] array) {
	    int N = array.length;
	    for (int i = 0; i < N; i++){
	    	int min = i;
	    	for (int j = i + 1; j < N; j++){
	    		comparisons++;
	    		if (less(array[j], array[min]))
	    		min = j;
	    		
	    	}
	    	exch(array, i, min);
	    }   
	    print(array);
	    sortedArray = new Comparable[array.length];
		sortedArray = array.clone();
	    return getCountComparisons();
    
  }

  private int getCountComparisons() {
		int comparisonsresult = comparisons;
		comparisons = 0;
		return comparisonsresult;
	}
  private int comparisons;
private static <T extends Comparable<T>> void exch(T[] array, int i, int j){
  	  T t = array[i]; 
  	  array[i] = array[j];
  	  array[j] = t;
  }
  private static <T extends Comparable<T>> boolean less(T v, T w){
  	  return v.compareTo(w) < 0;
  }
  private static <T extends Comparable<T>> void print(T[] array){
	  
	  for (int i=0; i< array.length; i++){
		  
		  System.out.print(array[i]);
		  System.out.print(" ");
	  }
	  System.out.println();
		  
  }
  
  /**
   * Constructor.
   */
  public SelectionSort(){
  }
}
