package gna;

import java.util.Random;
/**
 * 
 * @author Thomas De Clercq
 *
 */
public class Main {
 
  public static void main(String[] args) {
	  Random numberGenerator = new Random();
	  SelectionSort selectionSort = new SelectionSort();
	  InsertionSort insertionSort = new InsertionSort();
	  QuickSort quickSort = new QuickSort();
	  
	  int arrayLength = 100;
	  int amountOfExecutions = 100;
	  
	  int[] selectionResults = new int[amountOfExecutions];
	  int[] insertionResults = new int[amountOfExecutions];
	  int[] quickResults = new int[amountOfExecutions];

	  for (int execute = 0; execute<amountOfExecutions;execute++){
	  Integer[] array = new Integer[execute];
	  for (int i = 0; i < array.length; i++){
		  Integer integer = new Integer(numberGenerator.nextInt(array.length));
		  array[i] = integer;
		  }
	  
	  Integer[] arraySelection = new Integer[arrayLength];
	  Integer[] arrayInsertion = new Integer[arrayLength];
	  Integer[] arrayQuick = new Integer[arrayLength];


	  arraySelection = array.clone();
	  arrayInsertion = array.clone();
	  arrayQuick = array.clone();

	  
	  System.out.println("The unsorted array:");
	  print(array);
	  System.out.println();
	  System.out.println("The array sorted by SelectionSort:");
	  int selectionSortResult = selectionSort.sort(arraySelection);
	  selectionResults[execute] = selectionSortResult;
	  System.out.println(selectionSortResult);
	  System.out.println();
	  System.out.println("The array sorted by InsertionSort:");
	  int insertionSortResult = insertionSort.sort(arrayInsertion);
	  insertionResults[execute] = insertionSortResult;
	  System.out.println(insertionSortResult);
	  System.out.println();
	  System.out.println("The array sorted by QuickSort:");
	  int quickSortResult = quickSort.sort(arrayQuick);
	  quickResults[execute] = quickSortResult;
	  System.out.println(quickSortResult);
	  
	  Integer[] arrayDoubled = new Integer[array.length*2];
	  for (int i = 0; i < arrayDoubled.length; i++){
		  Integer integer = new Integer(numberGenerator.nextInt(arrayDoubled.length));
		  arrayDoubled[i] = integer;
		  }
	  /*
	  Integer[] arrayInsertionDoubled = new Integer[arrayLength];
	  Integer[] arrayQuickDoubled = new Integer[arrayLength];
	  
	  arrayInsertionDoubled = arrayDoubled.clone();
	  arrayQuickDoubled = arrayDoubled.clone();
	  System.out.println("-----------------------------------------");
	  System.out.println("DOUBLED RATIO");
	  System.out.println();
	  System.out.println("The unsorted array:");
	  print(arrayDoubled);
	  System.out.println("The doubled array sorted by InsertionSort:");
	  int insertionSortResultDoubled = insertionSort.sort(arrayInsertionDoubled);
	  insertionDoubledResults[execute] = insertionSortResultDoubled;
	  System.out.println(insertionSortResultDoubled);
	  System.out.println();
	  System.out.println("The doubled array sorted by QuickSort:");
	  int quickSortResultDoubled = quickSort.sort(arrayQuickDoubled);
	  quickDoubledResults[execute] = quickSortResultDoubled;
	  System.out.println(quickSortResultDoubled);
	*/

	  }
	  System.out.println();
	  System.out.println("SelectionSort total results: ");
	  for (int i = 0; i < amountOfExecutions; i++){
	  System.out.print(selectionResults[i] + "	");
	  }
	  System.out.println();
	  System.out.println("InsertionSort total results: ");
	  for (int i = 0; i < amountOfExecutions; i++){
	  System.out.print(insertionResults[i] + "	");
	  }
	  System.out.println();
	  System.out.println("QuickSort total results: ");
	  for (int i = 0; i < amountOfExecutions; i++){
	  System.out.print(quickResults[i] + "	");
	  }
	  int[] insertionDoubledResults = new int[10];
	  int[] quickDoubledResults = new int[10];
	  System.out.println();
	  System.out.println("-----------------------------------------");
	  System.out.println("       DOUBLING RATIO EXPERIMENT");
	  System.out.println("-----------------------------------------");
	  for (int i = 0; i < 5; i++){
		  Integer[] array = new Integer[(int) (100*Math.pow(2,i))];
		  Integer[] insertionSortArray = new Integer[array.length];
		  Integer[] quickSortArray = new Integer[array.length];
		  for (int j = 0; j < array.length; j++){
			  array[j] = numberGenerator.nextInt(insertionSortArray.length);
		  }
		  insertionSortArray = array.clone();
		  quickSortArray = array.clone();
		  System.out.println("The unsorted array:");
		  print(array);
		  System.out.println();
		  System.out.println("The array sorted by InsertionSort:");
		  int insertionDoubledResult = insertionSort.sort(insertionSortArray);
		  insertionDoubledResults[i] = insertionDoubledResult;
		  System.out.println(insertionDoubledResult);
		  System.out.println();
		  System.out.println("The array sorted by QuickSort:");
		  int quickDoubledResult = quickSort.sort(quickSortArray);
		  quickDoubledResults[i] = quickDoubledResult;
		  System.out.println(quickDoubledResult);
	  }
	  System.out.println();
	  System.out.println("InsertionSort total results: ");
	  for (int i = 0; i < 5; i++){
	  System.out.print(insertionDoubledResults[i] + "	");
	  }
	  System.out.println();
	  System.out.println("QuickSort total results: ");
	  for (int i = 0; i < 5; i++){
	  System.out.print(quickDoubledResults[i] + "	");
	  }
	  
	  
	  
  }
  private static <T extends Comparable<T>> void print(T[] array){
	  
	  for (int i=0; i< array.length; i++){
		  
		  System.out.print(array[i]);
		  System.out.print(" ");
	  }
	  System.out.println();
		  
  }
}

