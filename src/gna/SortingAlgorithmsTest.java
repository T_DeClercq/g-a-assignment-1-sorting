package gna;

import static org.junit.Assert.*;
import libpract.SortingAlgorithm;

import org.junit.Test;

/**
 * Tests for SortingAlgorithms.
 */
public class SortingAlgorithmsTest {
	final Integer[] array = {5,6,3,7,2,1,4,9,8};
	final Integer[] sortedArray = {1,2,3,4,5,6,7,8,9};
  
  @Test
  public void quickSortTest() {
	  QuickSort quickSort = new QuickSort();
	  quickSort.sort(array);
	  for (int i = 0; i < array.length; i++){
		  assertEquals(sortedArray[i], quickSort.sortedArray[i]);
	  }
  }
  @Test
  public void insertionSortTest() {
	  InsertionSort insertionSort = new InsertionSort();
	  insertionSort.sort(array);
	  for (int i = 0; i < array.length; i++){
		  assertEquals(sortedArray[i], insertionSort.sortedArray[i]);
	  }
  }
  @Test
  public void selectionSortTest() {
	  SelectionSort selectionSort = new SelectionSort();
	  selectionSort.sort(array);
	  for (int i = 0; i < array.length; i++){
		  assertEquals(sortedArray[i], selectionSort.sortedArray[i]);
	  }
  }
}
