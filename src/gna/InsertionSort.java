package gna;

/**
 * Performs sort by using the Selection Sort algorithm.
 *
 * @author Thomas De Clercq
 */
public class InsertionSort implements libpract.SortingAlgorithm{
  /**
   * Sorts the given array using insertion sort.
   * 
   * @return The number of comparisons (i.e. calls to compareTo) 
   * performed by the algorithm.
   */
  public Comparable sortedArray[];
  public <T extends Comparable<T>> int sort(T[] array) {
    int N = array.length;
    for (int i = 1; i < N; i++){
    	for (int j = i; j > 0 && less(array[j], array[j-1]); j--){
    		comparisons++;
    		exch(array, j, j-1);
    	}
    }
    print(array);
    sortedArray = new Comparable[array.length];
	sortedArray = array.clone();
    return getCountComparisons();
  }
  private int getCountComparisons() {
		double comparisonsresult = comparisons;
		comparisons = 0;
		return (int) comparisonsresult;
	}
  private double comparisons;
private static <T extends Comparable<T>> boolean less(T v, T w){
	  return v.compareTo(w) < 0;
  }
  private static <T extends Comparable<T>> void exch(T[] array, int i, int j){
	  T t = array[i]; 
	  array[i] = array[j];
	  array[j] = t;
  }
  private static <T extends Comparable<T>> void print(T[] array){
	  
	  for (int i=0; i< array.length; i++){
		  
		  System.out.print(array[i]);
		  System.out.print(" ");
	  }
	  System.out.println();
		  
  }
  
  /**
   * Constructor.
   */
  public InsertionSort(){
  }
}
